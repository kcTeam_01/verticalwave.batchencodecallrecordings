﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;

namespace BatchEncodeCallRecordings
{
    internal class Program
    {
        private static void EncodeFile(SqlConnection s_conn, FileData f)
        {
            var OldFilePath = String.Format(@"{0}\{1}\{2}\{3}", f.ArchivePath, f.ServerName, f.MailboxID, f.FileName);
            if (System.IO.File.Exists(OldFilePath))
            {
                var NewFilePath = OldFilePath.Replace(".wav", ".mp3");
                var NewFileName = f.FileName.Replace(".wav", ".mp3");

                var StartInfo = new System.Diagnostics.ProcessStartInfo("lame.exe", String.Format("-V5 \"{0}\" \"{1}\"", OldFilePath, NewFilePath));
                var p = System.Diagnostics.Process.Start(StartInfo);
                p.WaitForExit();

                if (System.IO.File.Exists(NewFilePath))
                {
                    using (var command = s_conn.CreateCommand())
                    {
                        command.CommandText = "UPDATE DBO.Message SET FileName=@NewFileName WHERE ID=@ID;";
                        command.Parameters.AddWithValue("NewFileName", NewFileName);
                        command.Parameters.AddWithValue("ID", f.ID);
                        command.ExecuteNonQuery();
                    }

                    System.IO.File.Delete(OldFilePath);
                }
            }
        }

        private static List<FileData> GetRecordingsToEncode(SqlConnection s_conn)
        {
            List<FileData> FilesToEncode = new List<FileData>();
            using (var command = s_conn.CreateCommand())
            {
                command.CommandText = @"
                    SELECT
	                    m.ID
	                    , s.ArchivePath
                        , m.ServerName
                        , cast(m.MailboxID as varchar(200)) AS MailboxID
                        , m.FileName
                    FROM
	                    [TVArchive].[dbo].[Settings] s
	                    , TVArchive.DBO.Message m
                    WHERE
	                    m.FileName like '%.wav'
                    ";
                using (var reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        FilesToEncode.Add(new FileData()
                        {
                            ArchivePath = reader["ArchivePath"].ToString(),
                            FileName = reader["FileName"].ToString(),
                            MailboxID = reader["MailboxID"].ToString(),
                            ServerName = reader["ServerName"].ToString(),
                            ID = Int32.Parse(reader["ID"].ToString()),
                        });
                    }
                }
            }
            return FilesToEncode;
        }

        private static void Main(string[] args)
        {
            var FilesToEncode = new List<FileData>();
            using (var s_conn = new SqlConnection("Server=AMPS;Database=TVArchive;User Id=tv_admin;Password=tv_admin;"))
            {
                s_conn.Open();
                FilesToEncode.AddRange(GetRecordingsToEncode(s_conn));

                for (int i = 0; i < FilesToEncode.Count; i++)
                {
                    Console.SetCursorPosition(0, 0);
                    Console.Write("{0} of {1}", i, FilesToEncode.Count);

                    var f = FilesToEncode[i];
                    EncodeFile(s_conn, f);
                }
            }
        }

        private struct FileData
        {
            public string ArchivePath { get; set; }

            public string FileName { get; set; }

            public int ID { get; set; }

            public string MailboxID { get; set; }

            public string ServerName { get; set; }
        }
    }
}